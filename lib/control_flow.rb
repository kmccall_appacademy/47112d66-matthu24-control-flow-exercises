# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char do |char|
    str.delete!(char) if char == char.downcase
  end
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  #find the middle index of the string
  length = str.length
  middle_index = length/2
  #return 1 or two letters based on whether or not the string
  #has an odd number or even number of
  if length % 2 == 1
    return str[middle_index]
  else
    return str[middle_index-1] + str[middle_index]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.each_char do |char|
    count += 1 if VOWELS.include?(char)
  end
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  return 1 if num == 0
  return factorial(num-1) * num
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  joined_string = ""
  arr.each_with_index do |char,index|
    joined_string += char
    joined_string += separator if index != arr.length-1
  end
  joined_string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"

def weirdcase(str)
  #index %2 == 0 is lowercase,
  #index %2 == 1 is uppercase
  weird_string = ""
  str.each_char.with_index do |char,index|
    weird_string += char.downcase if index % 2 == 0
    weird_string += char.upcase if index % 2 == 1
  end
  weird_string
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  word_array = str.split
  reverse_five_array = []
  word_array.each do |word|
    if word.length > 4
      reverse_five_array << word.reverse
    else
      reverse_five_array << word
    end
  end
  reverse_five_array.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  result_array = []
  (1..n).each do |num|
    if num % 3 == 0 && num % 5 == 0
      result_array << "fizzbuzz"
    elsif num % 3 == 0
      result_array << "fizz"
    elsif num % 5 == 0
      result_array << "buzz"
    else
      result_array << num
    end
  end
  result_array
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num < 2
  (2...num).each do |n|
    return false if num % n == 0
  end
  return true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  sorted_array = []
  (1..num).each do |n|
    sorted_array << n if num % n == 0
  end
  sorted_array.sort
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  all_factors = factors(num)
  prime_factors = []
  all_factors.each do |factors|
    prime_factors << factors if prime?(factors)
  end
  prime_factors
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)

def oddball(arr)
  #need to check the first three elements to see if the array is mostly odd or
  #mostly even
  even_count = 0
  odd_count = 0
  (0..2).each do |index|
    if arr[index] % 2 == 0
      even_count += 1
    else
      odd_count += 1
    end
  end

  arr.each do |num|
    if even_count > odd_count
      return num if num % 2 == 1
    else
      return num if num % 2 == 0
    end
  end

end
